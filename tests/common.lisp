(in-package :cl-naive-ptrees.tests)

(defun set-equal (a b &key (test (function eql)))
  (and (subsetp a b :test test)
       (subsetp b a :test test)))

(defun safe-elt (sequence index)
  (when (< -1 index (length sequence))
    (elt sequence index)))
