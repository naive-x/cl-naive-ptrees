(in-package :cl-naive-ptrees.tests)

(testsuite
 plist-to-values
 (testcase :typical
           :expected '(1 3)
           :actual (plist-to-values '(:x 1 :y 3))
           :info ""))

(testsuite
 plist-to-keys
 (testcase :typical
           :expected '(:x :y)
           :actual (plist-to-keys '(:x 1 :y 3))
           :info ""))

(testsuite
 plist-to-attributes
 (testcase :typical
           :expected '((:x 1) (:y 3))
           :actual (plist-to-attributes '(:x 1 :y 3))
           :info ""))

(defparameter *genealogy*
  (let* ((mahalath '(:name "Mahalath"
                     :children #((:name "Reuel"
                                  :wifes #(:children #((:name "Nahath")
                                                       (:name "Zerah")
                                                       (:name "Shammah")
                                                       (:name "Mizzah")))))))
         (leah    `(:name "Leah"
                    :children #((:name "Reuben")
                                (:name "Simeon")
                                (:name "Levi")
                                (:name "Judah")
                                (:name "Issachar")
                                (:name "Zebulun"))))
         (rachel  `(:name "Rachel"
                    :children #((:name "Joseph"
                                 :wifes #((:name "Asenath"
                                           :children #((:name "Manasseh")
                                                       (:name "Ephraim")))))
                                (:name "Benjamin"))))
         (rebekah `(:name "Rebekah"
                    :children #((:name "Esau"
                                 :wifes #(,mahalath
                                          (:name "Adah"
                                           :children #((:name "Eliphaz"
                                                        :wifes #((:children #((:name "Teman")
                                                                              (:name "Omar")
                                                                              (:name "Zepho")
                                                                              (:name "Gatam")
                                                                              (:name "Kenaz")))
                                                                 (:name "Timnah"
                                                                  :children #((:name "Amalek")))))))
                                          (:name "Aholibamah"
                                           :children #((:name "Jeush")
                                                       (:name "Jaalam")
                                                       (:name "Korah")))))
                                (:name "Jacob"
                                 :wifes #((:name "Zilpah"
                                           :children #((:name "Gad")
                                                       (:name "Asher")))
                                          ,leah
                                          ,rachel
                                          (:name "Bilhah"
                                           :children #((:name "Dan")
                                                       (:name "Naphtali"))))))))
         (terah `((:name "Terah"
                   :wifes #((:children #((:name "Abraham"
                                          :wifes #((:name "Keturah"
                                                    :children #((:name "Zimran")
                                                                (:name "Jokshan")
                                                                (:name "Medan")
                                                                (:name "Midian")
                                                                (:name "Ishbak")
                                                                (:name "Shuah")))
                                                   (:name "Hagar"
                                                    :children #((:name "Ishmael"
                                                                 :wifes #((:children #(,mahalath
                                                                                       (:name "Nebajoth")
                                                                                       (:name "Abdeel")
                                                                                       (:name "Mishma")
                                                                                       (:name "Massa")
                                                                                       (:name "Tema")
                                                                                       (:name "Naphish")
                                                                                       (:name "Kedar")
                                                                                       (:name "Mibsam")
                                                                                       (:name "Dumah")
                                                                                       (:name "Hadar")
                                                                                       (:name "Kedemah")
                                                                                       (:name "Jetur")))))))
                                                   (:name "Sarah"
                                                    :children #((:name "Isaac"
                                                                 :wifes #(,rebekah))))))
                                         (:name "Nahor"
                                          :wifes #((:name "Milcah"
                                                    :children #((:name "Huz")
                                                                (:name "Buz")
                                                                (:name "Kemuel")
                                                                (:name "Chesed")
                                                                (:name "Hazo")
                                                                (:name "Pildash")
                                                                (:name "Jidlaph")
                                                                (:name "Bethuel"
                                                                 :wifes #((:children #(,rebekah
                                                                                       (:name "Laban"
                                                                                        :wifes #((:children #(,leah ,rachel))))))))))))
                                         (:name "Haran"
                                          :children #((:name "Lot"
                                                       :children #((:name "Moab")
                                                                   (:name "Ammon"))))))))))))
    terah)
  "https://www.conformingtojesus.com/images/webpages/genealogy_of_abraham_1.jpg")

(defun has-with-name (name plists)
  (find name plists :key (lambda (plist) (getf plist :name)) :test #'equalp))


(testsuite
 walk-to-node
 (testcase :walk-to-string-1
           :equal    'equal
           :actual   (walk-to-node *genealogy* '(0 :wifes 0 :children 0 :name))
           :expected "Abraham"
           :info "Walking a plist-tree.")
 (testcase :walk-to-string-2
           :equal    'equal
           :actual   (walk-to-node *genealogy* '(0 :wifes 0 :children 0 :wifes 1 :children 0 :name))
           :expected "Ishmael"
           :info "Walking a plist-tree.")
 (testcase :walk-to-string-3
           :equal    'equal
           :actual   (walk-to-node *genealogy* '(0 :wifes 0 :children 1 :name))
           :expected "Nahor"
           :info "Walking a plist-tree.")
 (testcase :walk-to-string-4
           :equal    'equal
           :actual   (walk-to-node *genealogy* '(0 :wifes 0 :children 2 :name))
           :expected "Haran"
           :info "Walking a plist-tree.")
 (testcase :walk-to-sequence
           :equal    'cl-naive-ptrees::attribute-equal-p
           :actual   (map 'list (lambda (wife) (getf wife :name))
                          (walk-to-node *genealogy* '(0 :wifes 0 :children 0 :wifes 2 :children 0 :wifes 0 :children 1 :wifes)))
           :expected '("Zilpah" "Leah" "Rachel" "Bilhah")
           :info "Walking a plist-tree."))


(defun same-tree-with-matched-variables (expected actual variables)
  ;; Any occurences in the expected tree of a variable in the variables list
  ;; shall match any symbol in the actual tree (and identity should be checked).
  ;; otherwise the two trees expectecd and actual should be equal.
  (let ((variable-alist (mapcar (lambda (variable) (cons variable nil)) variables)))
    (labels ((fail () (return-from same-tree-with-matched-variables nil))
             (same-node (expected actual)
               (cond ((null expected)
                      (if (null actual)
                          t
                          (fail)))
                     ((symbolp expected)
                      (if (symbolp actual)
                          (let ((variable (assoc expected variable-alist)))
                            (if variable
                                (if (null (cdr variable))
                                    (setf (cdr variable) actual)
                                    (if (eq (cdr variable) actual)
                                        t
                                        (fail)))
                                (if (eq expected actual)
                                    t
                                    (fail))))
                          (fail)))
                     ((and (consp expected) (consp actual))
                      (or (and (same-node (car expected) (car actual))
                               (same-node (cdr expected) (cdr actual)))
                          (fail)))
                     (t
                      (or (equalp expected actual) (fail))))))
      (same-node expected actual))))

(assert (same-tree-with-matched-variables '(a (b c)) '(a (b c)) '()))
(assert (same-tree-with-matched-variables '(a x (b y x)) '(a ww (b zz ww)) '(x y)))
(assert (not (same-tree-with-matched-variables '(a x (b y x)) '(a ww (b zz vv)) '(x y))))


(testsuite
 cl-naive-ptrees::parse-criteria
 (testcase :too-many-indicators
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2) too many indicators))
           :expected nil
           :info "parse-criteria: too many indicators")
 (testcase :bad-criteria
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1 bad test) (:y 2)))
           :expected nil
           :info "parse-criteria: bad test")
 (testcase :test-dotted-list
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((not a proper . list) (:y 2)))
           :expected nil
           :info "parse-criteria: test not a proper list")
 (testcase :criteria-dotted-list
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2) . :z))
           :expected nil
           :info "parse-criteria: criteria not a proper list")
 (testcase :normal-without-wildcard
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (and
                          (let ((value
                                  (getf plist :x . #11=(absent))))
                            (and #12=(not (eql value absent))
                                 (equal value 1)))
                          (let ((value (getf plist :y . #11#)))
                            (and #12# (equal value 2))))))
           :info "parse-criteria: normal tests without wildcard")
 (testcase :normal-with-wildcard
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2) (:z)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (and
                          (let ((value (getf plist :x . #21=(absent))))
                            (and #22=(not (eql value absent))
                                 (equal value 1)))
                          (let ((value (getf plist :y . #21#)))
                            (and #22# (equal value 2)))
                          (not (eql (getf plist :z absent) absent)))))
           :info "parse-criteria: normal tests with wildcard")
 (testcase :only-wildcard
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria '((:z)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (and (not (eql (getf plist :z absent) absent)))))
           :info "parse-criteria: only an indicator")
 (testcase :disjunction-of-comparison
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria
                      '(or (:x 1) (:x 2) (< 3 :y) (:z)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (or
                          (let ((value (getf plist :x . #41=(absent))))
                            (and #42=(not (eql value absent))
                                 (equal value 1)))
                          (let ((value (getf plist :x . #41#)))
                            (and #42# (equal value 2)))
                          (let ((value (getf plist :y absent)))
                            (and (not (eql value absent))
                                 (< 3 value)))
                          (not (eql (getf plist :z absent) absent)))))
           :info "parse-criteria: disjunction with comparison tests and indicator"))

(testsuite
 cl-naive-ptrees::parse-criteria
 (testcase :too-many-indicators
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2) too many indicators))
           :expected nil
           :info "parse-criteria: too many indicators")
 (testcase :bad-criteria
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1 bad test) (:y 2)))
           :expected nil
           :info "parse-criteria: bad test")
 (testcase :test-dotted-list
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((not a proper . list) (:y 2)))
           :expected nil
           :info "parse-criteria: test not a proper list")
 (testcase :criteria-dotted-list
           :equal    'equal
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2) . :z))
           :expected nil
           :info "parse-criteria: criteria not a proper list")
 (testcase :normal-without-wildcard
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (and
                          (let ((value
                                  (getf plist :x . #11=(absent))))
                            (and #12=(not (eql value absent))
                                 (equal value 1)))
                          (let ((value (getf plist :y . #11#)))
                            (and #12# (equal value 2))))))
           :info "parse-criteria: normal tests without wildcard")
 (testcase :normal-with-wildcard
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria '((:x 1) (:y 2) (:z)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (and
                          (let ((value (getf plist :x . #21=(absent))))
                            (and #22=(not (eql value absent))
                                 (equal value 1)))
                          (let ((value (getf plist :y . #21#)))
                            (and #22# (equal value 2)))
                          (not (eql (getf plist :z absent) absent)))))
           :info "parse-criteria: normal tests with wildcard")
 (testcase :only-wildcard
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria '((:z)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (and (not (eql (getf plist :z absent) absent)))))
           :info "parse-criteria: only an indicator")
 (testcase :disjunction-of-comparison
           :equal    (lambda (expected actual)
                       (same-tree-with-matched-variables
                        expected actual '(plist absent value)))
           :actual   (cl-naive-ptrees::parse-criteria
                      '(or (:x 1) (:x 2) (< 3 :y) (:z)))
           :expected '(lambda (plist)
                       (declare (ignorable plist))
                       (let ((absent (cons nil nil)))
                         (declare (ignorable absent))
                         (or
                          (let ((value (getf plist :x . #41=(absent))))
                            (and #42=(not (eql value absent))
                                 (equal value 1)))
                          (let ((value (getf plist :x . #41#)))
                            (and #42# (equal value 2)))
                          (let ((value (getf plist :y absent)))
                            (and (not (eql value absent))
                                 (< 3 value)))
                          (not (eql (getf plist :z absent) absent)))))
           :info "parse-criteria: disjunction with comparison tests and indicator"))

(testsuite
    query
  (testcase :typical
            :equal    (lambda (a b) (set-equal a b :test #'equal))
            :expected '("Abraham" "Reuel" "Esau" "Reuel" "Eliphaz"
                        "Jacob" "Esau" "Reuel" "Eliphaz" "Jacob")
            :actual   (mapcar (lambda (plist) (getf plist :name))
                              (query *genealogy* '((safe-elt :wifes 1))))
            :info "Query the patriarchs with more than 1 wife without indicator")

  (testcase :nochain
            :equal    'equalp
            :expected '((:name "Mahalath" :children #((:name "Reuel" :wifes #(:children #((:name "Nahath") (:name "Zerah") (:name "Shammah") (:name "Mizzah"))))))
                        (:name "Mahalath" :children #((:name "Reuel" :wifes #(:children #((:name "Nahath") (:name "Zerah") (:name "Shammah") (:name "Mizzah"))))))
                        (:name "Mahalath" :children #((:name "Reuel" :wifes #(:children #((:name "Nahath") (:name "Zerah") (:name "Shammah") (:name "Mizzah")))))))
            :actual   (query *genealogy* '((has-with-name "Reuel" :children)))
            :info "Query the patriarchs that have Reuel as child")

  (testcase :chain
            :equal    'equalp
            :expected '((:name "Mahalath" :children #((:name "Reuel" :wifes #(:children #((:name "Nahath") (:name "Zerah") (:name "Shammah") (:name "Mizzah")))))))
            :actual   (query *genealogy* '(chain
                                           ((string= :name "Ishmael"))
                                           ((has-with-name "Reuel" :children))))
            :info "Query the patriarchs that have Reuel as child and Ishmael as ancestor"))

(testsuite
 traverse-plists
 (testcase :typical
           :equal    'equal
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1))
           :actual (let ((result '()))
                     (traverse-plists '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                        (:v 1 :w (:wa 1 :wb 1))
                                        (:x 1 :t 1))
                                      (lambda (plist)
                                        (push plist result))))
           :info "Test map-plist by incrementing the numbers of the plist that have only number values."))

(testsuite
 map-plists
 (testcase :increment-only-number-plists
           :equal    'equal
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 2 :bb 2)))
                       (:v 1 :w (:wa 2 :wb 2))
                       (:x 2 :t 2))
           :actual (map-plists
                    '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                      (:v 1 :w (:wa 1 :wb 1))
                      (:x 1 :t 1))
                    (lambda (plist)
                      (if (every (function numberp) (plist-to-values plist))
                          (loop :for (key value) :on plist :by (function cddr)
                                :collect key
                                :collect (if (numberp value)
                                             (1+ value)
                                             value))
                          plist)))
           :info "Test map-plist by building a new tree where the plist that have only number values are replaced by a similar plist with all the number incremented.")
  (testcase :increment-all-numbers
           :expected '((:x 2 :y 2 :z (:a 2 :b (:ba 2 :bb 2)))
                       (:v 2 :w (:wa 2 :wb 2))
                       (:x 2 :t 2 :z 2))
           :actual   (map-plists (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                              (:v 1 :w (:wa 1 :wb 1))
                                              (:x 1 :t 1 :z 1)))
                                 (lambda (plist)
                                   (map-into plist (lambda (element)
                                                     (if (numberp element)
                                                         (1+ element)
                                                         element))
                                             plist)))
           :info "Test map-plist by incrementing all the number values in the plist nodes."))

(testsuite
 traverse-attributes
 (testcase :typical
           :expected 10
           :actual (let ((pairs 0))
                     (traverse-attributes '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                            (:v 1 :w (:wa 1 :wb 1))
                                            (:x 1 :t 1))
                                          (lambda (attribute parent)
                                            (declare (ignore parent))
                                            (when (numberp (second attribute))
                                              (incf pairs (second attribute)))))
                     pairs)
           :info "Test tranverses the tree and counts the pair values where atoms."))

(testsuite
 map-attributes
 (testcase :typical
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1 :z 1))
           :actual (map-attributes
                    '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                      (:v 1 :w (:wa 1 :wb 1))
                      (:x 1 :t 1 :z 1))
                    (lambda (attribute parent)
                      (declare (ignore parent))
                      attribute))
           :info "Uses map-attribute to produce duplicate tree."))

(testsuite
 traverse-criteria
 (testcase :short
           :expected 4
           :actual (let ((pairs 0))
                     (traverse-criteria
                      '((:x 1))
                      '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                        (:v 1 :w (:wa 1 :wb 1))
                        (:x 1 :t 1 :z nil))
                      (lambda (attribute parent)
                        (declare (ignore parent))
                        (when (numberp (second attribute))
                          (incf pairs (second attribute)))))
                     pairs)
           :info "Test traverse-criteria the tree and counts the pair values where equal to path.")
 (testcase :long-count
           :expected 5
           :actual (let ((pairs 0))
                     (traverse-criteria
                      '((:x 1) (:z))
                      '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                        (:v 1 :w (:wa 1 :wb 1))
                        (:x 1 :t 1 :z 1))
                      (lambda (attribute parent)
                        (declare (ignore parent))
                        (when (numberp (second attribute))
                          (incf pairs (second attribute)))))
                     pairs)

           :info "Test traverse-criteria the tree and counts the pair values where equal to path.")
 (testcase :long-check-selected-plist
           :equal 'equal
           :expected '(((:c 3) / (:a 2 :b 2 :c 3))
                       ((:b 2) / (:a 2 :b 2 :c 3))
                       ((:a 2) / (:a 2 :b 2 :c 3)))
           :actual (let ((result '()))
                     (traverse-criteria '((:c 3) (:b 2) (:a))
                                        '((:a 1 :b 1 :c 3)
                                          (:a 2 :b 2 :c 3)
                                          (:a 100 :b 200 :c 300))
                                        (lambda (attribute parent)
                                          (push (list attribute '/ parent) result)))
                     result)
           :info "Test traverse-criteria the tree and check the right plist is selected.")
 (testcase :internal
           :expected 5
           :actual (let ((pairs 0))
                     (traverse-criteria
                      '((:z))
                      '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                        (:v 1 :w (:wa 1 :wb 1))
                        (:x 1 :t 1 :z 1))
                      (lambda (attribute parent)
                        (declare (ignore parent))
                        (when (numberp (second attribute))
                          (incf pairs (second attribute)))))
                     pairs)
           :info "Test traverse-criteria the tree and counts the pair values where equal to path."))

(testsuite
 map-criteria
 (testcase :typical
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1 :z 1))
           :actual (map-criteria
                    '((:x 1))
                    '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                      (:v 1 :w (:wa 1 :wb 1))
                      (:x 1 :t 1 :z 1))
                    (lambda (attribute parent)
                      (declare (ignore parent))
                      attribute))

           
           :info "Uses map-criteria to produce duplicate tree.")
 (testcase :long
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1 :z 1))
           :actual (map-criteria
                    '((:x 1) (:z))
                    '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                      (:v 1 :w (:wa 1 :wb 1))
                      (:x 1 :t 1 :z 1))
                    (lambda (attribute parent)
                      (declare (ignore parent))
                      attribute))
           :info "Uses map-criteria to produce duplicate tree.")
 (testcase :internal
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1 :z 1))
           :actual (map-criteria
                    '((:z))
                    '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                      (:v 1 :w (:wa 1 :wb 1))
                      (:x 1 :t 1 :z 1))
                    (lambda (attribute parent)
                      (declare (ignore parent))
                      attribute))
           :info "Uses map-criteria to produce duplicate tree."))

(testsuite
 append-attribute-value
 (testcase :typical
           :expected '((:x 1 :y 1 :z ((:a 1 :b (:ba 1 :bb 1)) #1=(:xx "NEW")))
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1 :z (#1#)))
           :actual (append-attribute-value
                    '((:x 1) (:z))
                    (copy-tree '((:x 1 :y 1 :z ((:a 1 :b (:ba 1 :bb 1))))
                                 (:v 1 :w (:wa 1 :wb 1))
                                 (:x 1 :t 1 :z nil)))
                    :z (list :xx "NEW"))
           :info "Uses append-attribute-value to append attribute to path attribute value."))

(testsuite
 update-attribute
 (testcase :typical
           :expected '((:x 1 :y 1 :z "NEW")
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :t 1 :z "NEW"))
           :actual (update-attribute
                    '((:x 1) (:z))
                    (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                 (:v 1 :w (:wa 1 :wb 1))
                                 (:x 1 :t 1 :z nil)))
                    :z "NEW")
           :info "Uses update-attribute to change the attribute value to path attribute value."))

(testsuite
 append-attribute
 (testcase :typical
           :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)) :xx "NEW")
                       (:v 1 :z "x" :w (:wa 1 :wb 1))
                       (:x 1 :duh 5 :z "p" :xx "NEW")
                       (:x 1 :t 1 :z nil :xx "NEW"))
           :actual (append-attribute
                    '((:x 1) (:z))
                    (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                 (:v 1 :z "x" :w (:wa 1 :wb 1))
                                 (:x 1 :duh 5 :z "p")
                                 (:x 1 :t 1 :z nil)))
                    :xx "NEW")
           :info "Uses append-attribute to append attribute to path attribute"))

(testsuite
    cl-naive-ptrees::query-places
  (testcase :typical
            :expected 3
            :actual (length (cl-naive-ptrees::query-places
                             '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                               (:v 1 :z "x" :w (:wa 1 :wb 1))
                               (:x 1 :duh 5 :z "p")
                               (:x 1 :t 1 :z nil))
                             '((:x 1) (:z))))
            :info "Test cl-naive-ptrees::query-places")

  (testcase :mutate
            :equal 'equalp
            :expected '((:x 1 :y 1 :z #(:zz 1) :foo 2)
                        (:v 1 :z "x" :w (:wa 1 :wb 1))
                        (:x 1 :duh 5 :z #(:zz 3) :foo 4)
                        (:x 1 :t 1 :z #(:zz 5) :foo 6))
            :actual (let ((ptree (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                              (:v 1 :z "x" :w (:wa 1 :wb 1))
                                              (:x 1 :duh 5 :z "p")
                                              (:x 1 :t 1 :z nil))))
                          (i 0))
                      (dolist (plist (cl-naive-ptrees::query-places ptree '((:x 1) (:z))))
                        (setf (getf (cl-naive-ptrees::deref plist) :z)  (vector :zz (incf i))
                              (cl-naive-ptrees::deref plist) (nconc (cl-naive-ptrees::deref plist) (list :foo (incf i)))))
                      ptree)
            :info "Test cl-naive-ptrees::query-places with mutation")

  (testcase :chain
            :equal 'equalp
            :expected '((:x 2 :duh 5 :z "p"))
            :actual (let ((ptree (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                              (:v 1 :z "x" :w (:wa 1 :wb 1))
                                              (:x 2 :duh 5 :z "p")
                                              (:x 3 :t 1 :z nil))))
                          (i 0))
                      (mapcar (function cl-naive-ptrees::deref)
                              (cl-naive-ptrees::query-places ptree '(chain ((:x 2)) ((:z))))))
            :info "Test cl-naive-ptrees::query-places with chain")

  (testcase :chain-mutate
            :equal 'equalp
            :expected '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                        (:v 1 :z "x" :w (:wa 1 :wb 1))
                        (:x 2 :z "pp")
                        (:x 3 :t 1 :z nil))
            :actual (let ((ptree (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                              (:v 1 :z "x" :w (:wa 1 :wb 1))
                                              (:x 2 :duh 5 :z "p")
                                              (:x 3 :t 1 :z nil))))
                          (i 0))

                      ;; NOTE: we cannot do (setf (cl-naive-ptrees::deref plist) (list :x 2 :z "pp"))
                      ;;       to replace the plist in the ptree list…
                      (mapc (lambda (plist)
                              (setf (cddr (cl-naive-ptrees::deref plist)) (list :z "pp")))
                            (cl-naive-ptrees::query-places ptree '(chain ((:x 2)) ((:z)))))
                      ptree)
            :info "Test cl-naive-ptrees::query-places with chain and mutation"))


(testsuite
 remove-attribute
 (testcase :remove-malcolm
           :expected '((:x 1 :y 1)
                       (:v 1 :z "x" :w (:wa 1 :wb 1))
                       (:x 1 :duh 5)
                       (:x 1 :t 1))
           :actual (remove-attribute
                    '((:x 1) (:z))
                    (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                 (:v 1 :z "x" :w (:wa 1 :wb 1))
                                 (:x 1 :duh 5 :z "p")
                                 (:x 1 :t 1 :z nil)))
                    :z)
           :info "Test remove-attribute")
 (testcase :remove-last-attribute
           :expected '((:x 1 :y 1)
                       (:v 1 :w (:wa 1 :wb 1))
                       (:x 1 :duh 5)
                       (:x 1 :t 1)
                       ())
           :actual (remove-attribute
                    '((:z))
                    (copy-tree '((:x 1 :y 1 :z (:a 1 :b (:ba 1 :bb 1)))
                                 (:v 1 :z "x" :w (:wa 1 :wb 1))
                                 (:x 1 :duh 5 :z "p")
                                 (:x 1 :t 1 :z nil)
                                 (:z 2)))
                    :z)
           :info "Uses remove-attribute to remove the last attribute of a plist"))

#-(and)
(progn
  (run)
  (report))
