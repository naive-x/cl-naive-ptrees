(in-package :cl-naive-ptrees.tests)

(testsuite issue-4
  
  (testcase :predicate-as-criteria
	    :expected '(t nil nil)
	    :actual (mapcar (function criteriap)
                            (list (function integerp)
                                  'integerp
                                  'foo))
	    :info "Test for criteria functions.")

  
  (testcase :use-predicate-as-criteria-2
            :equal    (lambda (a b) (set-equal a b :test #'equal))
	    :expected '((:name "foo" :in a :value 1)
                        (:name "foo" :in b :value 2))
	    :actual (query '(:a (:name "foo" :in a :value 1)
                             :b (:name "foo" :in b :value 2)
                             :c (:name "bar" :in c :balue 3))
                           (lambda (plist) (string= "foo" (getf plist  :name))))
	    :info "Test for criteria functions.")

  (testcase :use-predicate-as-criteria-3
            :equal    (lambda (a b) (set-equal a b :test #'equal))
	    :expected '((:name "bar" :value 33 :in a/foo)
                        (:name "bar" :in c :balue 3))
	    :actual (query '(:a (:name "foo" :in a :value (:name "bar" :value 33 :in a/foo))
                             :b (:name "foo" :in b :value 2)
                             :c (:name "bar" :in c :balue 3))
                           (lambda (plist) (string= "bar" (getf plist  :name))))
	    :info "Test for criteria functions plist in plist."))



