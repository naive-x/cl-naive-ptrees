(in-package :common-lisp-user)

(defpackage :cl-naive-ptrees.tests
  (:use :cl :cl-naive-ptrees :cl-naive-tests)
  (:export
   :run
   :report))
