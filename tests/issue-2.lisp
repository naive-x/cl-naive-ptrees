(in-package :common-lisp-user)

(defpackage :cl-naive-ptrees.tests.issue-2
  (:use :cl :cl-naive-ptrees :cl-naive-tests)
  (:export
   :run
   :report))

(in-package :cl-naive-ptrees.tests.issue-2)


(defparameter *plists-with-list*    '((:x 1 :y 2 :z (:a 3 :b (:ba 4 :bb 5)))
                                      (:v 6 :w (:wa 7 :wb 8))
                                      (:x 9 :t 10 :z foo)
                                      (:a 44 :b ((:x 41 :y 42 :z 43)
                                                 (:x 31 :y 32 :z 33)
                                                 (:x 21 :y 22 :z 23)))))
(defparameter *plists-with-vector*  '((:x 1 :y 2 :z (:a 3 :b (:ba 4 :bb 5)))
                                      (:v 6 :w (:wa 7 :wb 8))
                                      (:x 9 :t 10 :z foo)
                                      (:a 44 :b #((:x 41 :y 42 :z 43)
                                                  (:x 31 :y 32 :z 33)
                                                  (:x 21 :y 22 :z 23)))))

(defparameter *walk-to-node-test-cases*
  '(((0 :x) 1)
    ((0 :y) 2)
    ((0 :z) (:a 3 :b (:ba 4 :bb 5)))
    ((0 :z :a)  3)
    ((0 :z :b)  (:ba 4 :bb 5))
    ((0 :z :b :ba) 4)
    ((0 :z :b :bb) 5)
    ((1 :v) 6)
    ((1 :w) (:wa 7 :wb 8))
    ((1 :w :wa) 7)
    ((1 :w :wb) 8)
    ((2 :x) 9)
    ((2 :t) 10)
    ((2 :z) foo)
    ((3 :a) 44)
    ((3 :b 0 :x) 41)
    ((3 :b 0 :y) 42)
    ((3 :b 0 :z) 43)
    ((3 :b 1 :x) 31)
    ((3 :b 1 :y) 32)
    ((3 :b 1 :z) 33)
    ((3 :b 2 :x) 21)
    ((3 :b 2 :y) 22)
    ((3 :b 2 :z) 23)))


(testsuite  issue-2

  (testcase :issue-2-list
            :equal    'equal
            :actual   (loop :for (path nil) :in *walk-to-node-test-cases*
                            :for actual := (walk-to-node *plists-with-list* path)
                            :collect actual)
            
            :expected (loop :for (nil expected) :in *walk-to-node-test-cases*
                            :collect expected)
            :info "Walking a plist-tree with lists.")

  (testcase :issue-2-vector
            :equal    'equal
            :actual   (loop :for (path nil) :in *walk-to-node-test-cases*
                            :for actual := (walk-to-node *plists-with-vector* path)
                            :collect actual)
            
            :expected (loop :for (nil expected) :in *walk-to-node-test-cases*
                            :collect expected)
            :info "Walking a plist-tree with vectors.")
  )


#-(and) (progn (ql:quickload "cl-naive-ptrees.tests") (cl-naive-tests:write-results (cl-naive-tests:run :debug cl-naive-tests:*debug*) :format :text))
