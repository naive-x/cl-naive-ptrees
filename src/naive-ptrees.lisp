(in-package :cl-naive-ptrees)

(defun proper-list-p (object)
  "Return whether OBJECT is a proper list.
NOTE: terminates with any kind of list, dotted, or circular.
  "
  (and (listp object)
       (loop
         :named proper
         :for current = object :then (cddr current)
         :for slow = (cons nil object) :then (cdr slow)
         :do (cond
               ((null current)       (return-from proper t))
               ((atom current)       (return-from proper nil))
               ((null (cdr current)) (return-from proper t))
               ((atom (cdr current)) (return-from proper nil))
               ((eq current slow)    (return-from proper nil))))))

(defun indicatorp (object)
  (symbolp object))

(defun plistp (object)
  (and (proper-list-p object)
       (evenp (length object))
       (loop
         :for (key nil) :on object :by (function cddr)
         :always (indicatorp key))))

(defun pathp (object)
  (and (proper-list-p object)
       (every (lambda (element) (or (indicatorp element)
                                    (integerp element)))
              object)))

;; We cache the result of parse-criteria, since it's used as criteriap
;; so that (progn (check-type criteria criteria) (parse-criteria criteria))
;; doesn't waste time.

(defparameter *parse-criteria-cache* (cons nil nil))

(defun parse-criteria (criteria)
  (if (eql (car *parse-criteria-cache*) criteria)
      (cdr *parse-criteria-cache*)
      (setf (car *parse-criteria-cache*) criteria
            (cdr *parse-criteria-cache*)
            (block parse-criteria
              (when (proper-list-p criteria)
                (let ((op        'and)
                      (tests     '())
                      (vplist    (gensym "plist"))
                      (vabsent   (gensym "absent"))
                      (vvalue    (gensym "value")))
                  (when (member (first criteria) '(and or))
                    (setf op (pop criteria)))
                  (dolist (test-or-indicator criteria)
                    (if (proper-list-p test-or-indicator)
                        ;;TODO: Test expanded criteria takes function
                        ;;instead of just compare function.
                        (if (consp (first test-or-indicator))
                            (progn
                              (push `(let ((,vvalue (getf ,vplist ,(second test-or-indicator)
                                                          ,vabsent)))
                                       (and (not (eql ,vvalue ,vabsent))
                                            (funcall ,(first test-or-indicator)
                                                     ,vvalue)))
                                    tests))
                            (case (length test-or-indicator)
                              ((1)
                               (unless (indicatorp (first test-or-indicator))
                                 (return-from parse-criteria nil))
                               (push `(not (eql (getf ,vplist ,(first test-or-indicator)
                                                      ,vabsent)
                                                ,vabsent))
                                     tests))
                              ((2)
                               (unless (indicatorp (first test-or-indicator))
                                 (return-from parse-criteria nil))
                               (push `(let ((,vvalue (getf ,vplist ,(first test-or-indicator)
                                                           ,vabsent)))
                                        (and (not (eql ,vvalue ,vabsent))
                                             (equal ,vvalue ,(second test-or-indicator))))
                                     tests))
                              ((3)
                               (unless (and (symbolp (first test-or-indicator))
                                            (or (indicatorp (second test-or-indicator))
                                                (indicatorp (third test-or-indicator))))
                                 (return-from parse-criteria nil))
                               (let ((compare    (first test-or-indicator))
                                     (indicator  (if (indicatorp (second test-or-indicator))
                                                     (second test-or-indicator)
                                                     (third test-or-indicator)))
                                     (expression (if (indicatorp (second test-or-indicator))
                                                     (third test-or-indicator)
                                                     (second test-or-indicator))))
                                 (push `(let ((,vvalue (getf ,vplist ,indicator ,vabsent)))
                                          (and (not (eql ,vvalue ,vabsent))
                                               (,compare ,@(if (indicatorp (second test-or-indicator))
                                                               (list vvalue expression)
                                                               (list expression vvalue)))))
                                       tests)))
                              (otherwise
                               (return-from parse-criteria nil))))
                        (return-from parse-criteria nil)))
                  `(lambda (,vplist)
                     (declare (ignorable ,vplist))
                     (let ((,vabsent (cons nil nil)))
                       (declare (ignorable ,vabsent))
                       (,op ,@(nreverse tests))))))))))

(defun criteriap (object)
  (or (functionp object)
      (parse-criteria object)))

(deftype proper-list () '(satisfies proper-list-p))
(deftype indicator   () '(satisfies indicatorp))
(deftype plist       () '(satisfies plistp))
(deftype path        () '(satisfies pathp))
(deftype criteria    () '(satisfies criteriap))

(defun plist-to-keys (plist)
  "Returns the keys of a plist as a list."
  (unless (plistp plist)
    (error 'type-error :datum plist :expected-type 'plist))
  (loop :for (key) :on plist :by #'cddr
        :collect key))

(defun plist-to-values (plist)
  "Returns the values of a plist as a list."
  (unless (plistp plist)
    (error 'type-error :datum plist :expected-type 'plist))
  (loop :for (nil value) :on plist :by #'cddr
        :collect value))

(defun plist-to-attributes (plist)
  "Returns a list of key value pair lists of a plist."
  (unless (plistp plist)
    (error 'type-error :datum plist :expected-type 'plist))
  (loop :for (key value) :on plist :by #'cddr
        :collect (list key value)))

(define-condition indicator-absent-error (error)
  ((plist     :initarg :plist     :reader indicator-absent-plist)
   (indicator :initarg :indicator :reader indicator-absent-indicator))
  (:report (lambda (condition stream)
             (format stream "Indicator ~S is not found in the plist ~S"
                     (indicator-absent-indicator condition)
                     (indicator-absent-plist condition)))))

(defun walk-to-node (plist-tree path)
  (check-type path path)
  (labels ((walk (node path)
             ;; note: we don't have to validate the path,
             ;; since check-type already did it.
             (cond
               ((null path)
                node)
               ((indicatorp (car path))
                (unless (plistp node)
                  (error 'type-error
                         :datum node
                         :expected-type 'plist))
                (let ((value (getf node (car path) '#1=#:absent)))
                  (if (eql value '#1#)
                      (error 'indicator-absent-error
                             :plist node
                             :indicator (car path))
                      (walk value (cdr path)))))
               (t
                (unless (or (proper-list-p node) (vectorp node))
                  (error 'type-error
                         :datum node
                         :expected-type 'sequence))
                (let ((length (length node)))
                  (if (< -1 (car path) length)
                      (walk (elt node (car path)) (cdr path))
                      (error 'type-error
                             :datum (car path)
                             :expected-type `(integer 0 ,(1- length)))))))))
    (walk plist-tree path)))

(defmacro & (place)
  `(lambda (&optional (new-value nil new-value-p))
     (if new-value-p
         (setf ,place new-value)
         ,place)))

(defun (setf deref) (new-value place) (funcall place new-value))
(defun deref (place) (funcall place))

(defun traverse-plist-places (plist-tree plist-fun)
  "Walk the PLIST-TREE, and call PLIST-FUN with the place of each plist in the tree.
Return the plist-tree."
  (labels ((recurse (node)
             (cond
               ((plistp node)
                (loop :for current :on node :by #'cddr
                      :do (let ((current current))
                            (when (plistp (cadr current))
                              (funcall plist-fun (& (cadr current)))))
                          (recurse (cadr current))))
               ((proper-list-p node)
                (loop :for current :on node
                      :do (let ((current current))
                            (when (plistp (car current))
                              (funcall plist-fun (& (car current)))))
                          (recurse (car current))))
               ((vectorp node)
                (dotimes (i (length node))
                  (let ((i i))
                    (when (plistp (aref node i))
                      (funcall plist-fun (& (aref node i)))))
                  (recurse (aref node i)))))))
    (when (plistp plist-tree)
      (funcall plist-fun (& plist-tree)))
    (recurse plist-tree)
    plist-tree))

(defun traverse-plists (plist-tree plist-fun)
  "Walk the PLIST-TREE, and call PLIST-FUN on each plist in the tree.
Return the plist-tree."
  (cond
    ((plistp plist-tree)
     (funcall plist-fun plist-tree)
     (loop :for (nil value) :on plist-tree :by #'cddr
           :do (traverse-plists value plist-fun)))
    ((proper-list-p plist-tree)
     (dolist (element plist-tree)
       (traverse-plists element plist-fun)))
    ((vectorp plist-tree)
     (dotimes (i (length plist-tree))
       (traverse-plists (aref plist-tree i) plist-fun))))
  plist-tree)

(defun map-plists (plist-tree plist-fun)
  "Walk the PLIST-TREE, and call PLIST-FUN on each plist in the tree,
building a similar tree with the results of PLIST-FUN."
  (cond
    ((plistp plist-tree)
     (let ((result (funcall plist-fun plist-tree)))
       (if (eql result plist-tree)
           (loop :for (key value) :on plist-tree :by #'cddr
                 :collect key
                 :collect (map-plists value plist-fun))
           result)))
    ((proper-list-p plist-tree)
     (map 'list (lambda (element) (map-plists element plist-fun)) plist-tree))
    ((vectorp plist-tree)
     (map 'vector (lambda (element) (map-plists element plist-fun)) plist-tree))
    (t
     plist-tree)))

(defun query (plist-tree criteria)
  "Return a list of plists or values in the PLIST-TREE that match the CRITERIA."
  (let ((chain)
        (result-tree plist-tree)
        (final-results))

    (when (and (listp criteria)
               (symbolp (first criteria))
               (string-equal (first criteria) 'chain))
      (setf chain (cdr criteria)))

    (dolist (criteria-x (or chain (list criteria)))
      (check-type criteria-x criteria)

      (let ((test     (if (functionp criteria-x)
                          criteria-x
                          (coerce (parse-criteria criteria-x) 'function)))
            (results '()))
        (traverse-plists result-tree
                         (lambda (plist)
                           (when (funcall test plist)
                             (push plist results))))
        (if results
            (setf result-tree results)
            (return-from query nil))
        (push results final-results)))
    (first final-results)))

(defun query-places (plist-tree criteria)
  "Return a list of plists or values in the PLIST-TREE that match the CRITERIA.

Note: If CRITERIA is a CHAIN, then it's equivalent to:
    (query-places (query plist-tree (butlast criteria))
                  (first (last criteria)))
"
  (if (and (listp criteria)
           (symbolp (first criteria))
           (string-equal (first criteria) 'chain))

      (query-places (query plist-tree (butlast criteria))
                    (first (last criteria)))

      (progn
        (check-type criteria criteria)
        (let ((test     (if (functionp criteria)
                            criteria
                            (coerce (parse-criteria criteria) 'function)))
              (results '()))
          (traverse-plist-places plist-tree
                                 (lambda (plist-place)
                                   (when (funcall test (deref plist-place))
                                     (push plist-place results))))
          (nreverse results)))))

(defun traverse-attributes (plist-tree attribute-func)
  "Traverses the plist-tree and calls the attribute-func for each attribute pair."
  (labels ((recurse (node)
             (cond ((plistp node)
                    (loop :for (key value) :on node :by #'cddr
                          :do (when attribute-func
                                (funcall attribute-func (list key value) node))
                              (when (typep value 'sequence)
                                (recurse value))))
                   ((consp node)
                    (recurse (car node))
                    (recurse (cdr node)))
                   ((or (stringp node) (bit-vector-p node)))
                   ((vectorp node)
                    (dotimes (i (length node))
                      (recurse (aref node i)))))))
    (recurse plist-tree)))

(defun map-attributes (plist-tree attribute-func)
  "Traverses the plist tree and calls the attribute-func for each attribute pair and uses the values returned from the attribute-func to build a new plist-tree."
  (labels ((recurse (node)
             (cond ((plistp node)
                    (loop
                      :for (key value) :on node :by #'cddr
                      :append (let* ((attribute (list key value))
                                     (sub-plist (if attribute-func
                                                    (funcall attribute-func attribute node)
                                                    attribute)))
                                ;; If the attirbute-func didn't change the attribute,
                                ;; then recurse on the value.
                                ;; else we don't recurse (assume it gave us the good one)
                                (if (eql attribute sub-plist)
                                    ;; not changed
                                    (list key (recurse value))
                                    sub-plist))))
                   ((consp node)
                    (let ((car-cons (recurse (car node)))
                          (cdr-cons (recurse (cdr node))))
                      (if car-cons
                          (cons car-cons cdr-cons)
                          cdr-cons)))
                   ((or (stringp node) (bit-vector-p node))
                    node)
                   ((vectorp node)
                    (map 'vector (function recurse) node))
                   (t
                    node))))
    (recurse plist-tree)))

(defgeneric attribute-equal-p (a b)
  (:method ((a t) (b t))
    (equal a b))
  (:method ((a number) (b number))
    (= a b))
  (:method ((a cons) (b cons))
    (or (eql a b)
        (and (attribute-equal-p (car a) (car b))
             (attribute-equal-p (cdr a) (cdr b)))))
  (:method ((a string) (b string))
    (string= a b))
  (:method ((a vector) (b vector))
    (or (eql a b)
        (and (= (length a) (length b))
             (loop :for i :below (length a)
                   :always (attribute-equal-p (aref a i)
                                              (aref b i))))))
  (:method ((a array) (b array))
    (or (eql a b)
        (and (equal (array-dimensions a) (array-dimensions b))
             (loop :for i :below (array-total-size a)
                   :always (attribute-equal-p (row-major-aref a i)
                                              (row-major-aref b i)))))))

(defun traverse-criteria (criteria plist-tree attribute-func)
  "Traverses the plist-tree and calls the attribute-func for each attribute pair that matches the criteria supplied.
The criteria selects plists in the plist-tree.  The ATTRIBUTE-FUNC is called one each attribute of each selected plist.

Note: all the attribute in the criteria are matched into a single plist, to select it.

An item in the criteria can be specific like (:att \"x\") which will match all attribute with name = :att and value = \"x\".
Or the item in the criteria can be non specific (:att) which will match all attributes with then name :att.
Specific and non specific criteria elements can be mixed like ((:att \"x\") (:other-att))
"
  (dolist (plist (query plist-tree criteria))
    (loop :for (key value) :on plist :by (function cddr)
          :do (funcall attribute-func (list key value) plist))))

(defun map-criteria (criteria plist-tree attribute-func)
  "Traverses the plist-tree and calls the attribute-func for each attribute pair in plists that matches the criteria and uses the values returned from the attribute-func to build a new plist-tree.
Pairs that are not affected by attribute-func are used as they are to build the new/changed tree."

  ;; If we want to use a chain criteria to select the attributes to map,
  ;; we'll need to do it in two phases:
  ;;   1- select the attributes to map, using query (or query-place?)
  ;;   2- map the original plist-tree.

  ;; I'm not sure that would be needed to allow chain criteria in map-criteria.

  (if (and (listp criteria)
           (symbolp (first criteria))
           (string-equal (first criteria) 'chain))
      (let ((selected-nodes (query plist-tree criteria)))
        (map-attributes plist-tree
                        (lambda (attribute plist)
                          (if (member plist selected-nodes)
                              (funcall attribute-func attribute plist)
                              attribute))))
      (progn
        (check-type criteria criteria)
        (let ((test (if (functionp criteria)
                        criteria
                        (coerce (parse-criteria criteria) 'function))))
          (map-attributes plist-tree
                          (lambda (attribute plist)
                            (if (funcall test plist)
                                (funcall attribute-func attribute plist)
                                attribute)))))))

(define-modify-macro appendf (&rest lists) append)

(defun extract (sequence nth start end)
  (cond
    (nth
     (list (elt sequence nth)))
    ((and (zerop start) (null end)) sequence)
    (t
     (subseq sequence start end))))

(defun append-attribute-value (criteria plist-tree indicator value
                               &key (nth nil) (start 0) (end nil))
  "Selects plists in PLIST-TREE matching the criteria.
If NTH is given, then only the NTH plist is modified,
otherwise only the plists between START and END are modified.
The VALUE is appended to the value of the attribute indicated by INDICATOR."
  (dolist (plist (extract (query plist-tree criteria) nth start end) plist-tree)
    (when plist
      (setf (getf plist indicator nil)
            (nconc (getf plist indicator nil) (list value))))))

(defun update-attribute (criteria plist-tree indicator value
                         &key (nth nil) (start 0) (end nil))
  "Selects plists in PLIST-TREE matching the criteria.
If NTH is given, then only the NTH plist is modified,
otherwise only the plists between START and END are modified.
Changes the value of attribute for attributes matched by criteria."
  (dolist (plist (extract (query plist-tree criteria) nth start end) plist-tree)
    (when plist
      (setf (getf plist indicator) value))))

(defun append-attribute (criteria plist-tree indicator value
                         &key (nth nil) (start 0) (end nil))
  "Selects plists in PLIST-TREE matching the criteria.
If NTH is given, then only the NTH plist is modified,
otherwise only the plists between START and END are modified.
Appends an attribute to selected plists."
  (dolist (plist (extract (query plist-tree criteria) nth start end) plist-tree)
    (when plist
      (nconc plist (list indicator value)))))

(defun remove-attribute (criteria plist-tree indicator
                         &key (nth nil) (start 0) (end nil))
  "Selects plists in PLIST-TREE matching the criteria.
If NTH is given, then only the NTH plist is modified,
otherwise only the plists between START and END are modified.
Removes the attribute from the selected plists."
  (let ((tree (list plist-tree)))
    (dolist (plist-place (extract (query-places plist-tree criteria) nth start end)
                         (car tree))
      (when (deref plist-place)
        (remf (deref plist-place) indicator)))))

