(in-package :common-lisp-user)

(defpackage :cl-naive-ptrees
  (:use :cl)
  (:export

   :proper-list-p :proper-list
   :indicatorp :indicator
   :plistp :plist
   :pathp :path
   :criteriap :criteria
   :indicator-absent-error :indicator-absent-plist :indicator-absent-indicator
   :plist-to-keys
   :plist-to-values
   :plist-to-attributes
   :walk-to-node
   :query
   :traverse-plists
   :map-plists
   :traverse-attributes
   :map-attributes
   :traverse-criteria
   :map-criteria
   :append-attribute-value
   :update-attribute
   :append-attribute
   :remove-attribute
   ))
