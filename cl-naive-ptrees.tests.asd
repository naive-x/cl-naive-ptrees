(defsystem "cl-naive-ptrees.tests"
  :description "Tests for cl-naive-ptrees"
  :version "2023.8.22"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-ptrees :cl-naive-tests)
  :components (
	       (:file "tests/package")
	       (:file "tests/common"  :depends-on ("tests/package"))
	       (:file "tests/test"    :depends-on ("tests/package" "tests/common"))
               (:file "tests/issue-4" :depends-on ("tests/package" "tests/common"))
               (:file "tests/issue-2")))

